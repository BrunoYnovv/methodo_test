import * as readline from 'readline';

export class MaConsole {
    
    constructor(){
        // Démarrage de la console
        console.log("Bonjour");
        
        // Echange utilisateur console
        /*
        let userInput = "";
        
        userInput = input("Enter name: ");
        console.log(userInput);
        */
       
       let rl = readline.createInterface({
           input: process.stdin,
           output: process.stdout
        });
        
        rl.question('Is this example useful? [y/n] ', (answer) => {
        switch(answer.toLowerCase()) {
            case 'y':
                console.log('Super!');
                break;
            case 'n':
                console.log('Sorry! :(');
                break;
            default:
                console.log('Invalid answer!');
            }
            rl.close();
        });

        // Extinction de la console
        console.log("Aurevoir");
    }
}